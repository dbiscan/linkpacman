﻿using UnityEngine;
using System.Collections;

public class BlueSnakeScript : MonoBehaviour {
	public Transform[] waypoints;
	int cur = 0;
	public float speed = 1.0f;

	public GameObject Link;
	public Transform findlink;
	public Animator dieLink;
	public Rigidbody2D enemyBody;

	void Start(){

		Link = GameObject.Find ("linkWalkingPlain_0");
		dieLink = Link.GetComponent<Animator> ();
		enemyBody = GetComponent<Rigidbody2D>();
	}
	

	void FixedUpdate () {

		findlink = Link.transform;

		var heading = transform.position - findlink.position;
		var nextWay = transform.position - waypoints [cur].position;
		//var distanceLink = heading.magnitude;
		//var distanceWay = nextWay.magnitude;
		//AI for the enemies was pretty tough. I just made them static paths.

		// Waypoint not reached yet? then move closer
 		if (transform.position != waypoints [cur].position){ //&& (distanceWay < distanceLink)) {
			Vector2 p = Vector2.MoveTowards (transform.position, waypoints [cur].position, speed);
			enemyBody.MovePosition (p);
		} 
		else{
			// Waypoint reached, select next one
			cur = (cur + 1) % waypoints.Length;
		} 

		// Animation
		Vector2 dir = waypoints[cur].position - transform.position;
		GetComponent<Animator>().SetFloat("DirX", dir.x);
		GetComponent<Animator>().SetFloat("DirY", dir.y);
	}



	void OnTriggerEnter2D(Collider2D co) {

		if (co.name == "linkWalkingPlain_0" && !dieLink.GetBool("tempInvinc")) {
			dieLink.SetBool("Dead", true);
			Link.layer = 4;

		}
	}

}
