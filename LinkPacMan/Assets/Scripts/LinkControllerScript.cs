﻿using UnityEngine;
using System.Collections;


public class LinkControllerScript : MonoBehaviour {

	public float maxSpeed = 10f;
	public Animator movelinkmove;
	float speed = 0.01f;
	public GameObject reSpawn;
	public Transform reSpa;
	public bool enableMovement = true;
	public int HeartsPlus;
	public int HeartsMinus;

	public GameObject heart0;
	public GameObject heart1;
	public GameObject heart2;

	public Animator heart0_0;
	public Animator heart1_0;
	public Animator heart2_0;

	public int deadTimes;
	public int score;
	public int score2;
	public GameObject scorre;
	public GUIText scoreText;
	public GameObject dieSound;
	public AudioSource DieDie;
	public GameObject hurtSound;
	public AudioSource HURT;

	void Start () {

		Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
		movelinkmove = GetComponent<Animator> ();
		reSpawn = GameObject.Find ("RespawnPoint");
		reSpa = reSpawn.transform;

		heart0 = GameObject.Find ("Hearts_0");
		heart1 = GameObject.Find ("Hearts_1");
		heart2 = GameObject.Find ("Hearts_2");

		heart0_0 = heart0.GetComponent<Animator> ();
		heart1_0 = heart1.GetComponent<Animator> ();
		heart2_0 = heart2.GetComponent<Animator> ();
		dieSound = GameObject.Find ("Die");
		DieDie = dieSound.GetComponent<AudioSource> ();
		hurtSound = GameObject.Find ("Hurt");
		HURT = hurtSound.GetComponent<AudioSource> ();

		deadTimes = 0;
		UpdateScore ();

	}

	IEnumerator Wait2SecsOnDie() {
		float seconds = 1.8f;

		if (deadTimes == 1) {
			heart2_0.SetBool("heartempty",true);
			heart2_0.SetBool("heartfull",false);

		}
		else if (deadTimes == 2) {
			heart1_0.SetBool("heartempty",true);
			heart1_0.SetBool("heartfull",false);
		}
		else if (deadTimes == 3) {
			heart0_0.SetBool("heartempty",true);
			heart0_0.SetBool("heartfull",false);
			DieDie.Play();
			yield return new WaitForSeconds(3);
			Application.LoadLevel("GameOver");
		}

		yield return new WaitForSeconds(seconds);
		transform.position = reSpa.position;
		//movelinkmove.SetBool("Dead", false);
		movelinkmove.SetBool("Respawn", true);
		enableMovement=true;
		movelinkmove.SetBool("tempInvinc", false);
		this.gameObject.layer = 0;
	}

	void UpdateScore ()
	{
		//score=score+10;
		//score = score;
		score = movelinkmove.GetInteger ("ScoreCounter");
		scoreText.text = "Score: " + score;
	}
	// Update is called once per frame
	void FixedUpdate () {

		UpdateScore ();
		if (movelinkmove.GetBool("Dead")){
			movelinkmove.SetBool("Dead", false);
			enableMovement = false;
			movelinkmove.SetBool("tempInvinc", true);
			HURT.Play();
			movelinkmove.Play("LinkDead");
			deadTimes = deadTimes+1;
			StartCoroutine(Wait2SecsOnDie());

		}
		if (Input.GetKey (KeyCode.Escape)) {
			Application.LoadLevel ("Menu");
		}
		if (enableMovement) {

			if (Input.GetKey (KeyCode.LeftArrow)) {
				movelinkmove.SetInteger ("down", 0);
				movelinkmove.SetInteger ("Up", 0);
				movelinkmove.SetInteger ("left", 1);
				movelinkmove.SetInteger ("right", 0);
				movelinkmove.SetInteger ("Speeed", 1);
				transform.position += Vector3.left * speed;// * Time.deltaTime;
			} else if (Input.GetKey (KeyCode.RightArrow)) {
				movelinkmove.SetInteger ("down", 0);
				movelinkmove.SetInteger ("Up", 0);
				movelinkmove.SetInteger ("left", 0);
				movelinkmove.SetInteger ("right", 1);
				movelinkmove.SetInteger ("Speeed", 1);
				transform.position += Vector3.right * speed;// * Time.deltaTime;
			} else if (Input.GetKey (KeyCode.UpArrow)) {
				movelinkmove.SetInteger ("down", 0);
				movelinkmove.SetInteger ("Up", 1);
				movelinkmove.SetInteger ("left", 0);
				movelinkmove.SetInteger ("right", 0);
				movelinkmove.SetInteger ("Speeed", 1);
				transform.position += Vector3.up * speed;// * Time.deltaTime;
			} else if (Input.GetKey (KeyCode.DownArrow)) {
				movelinkmove.SetInteger ("down", 1);
				movelinkmove.SetInteger ("Up", 0);
				movelinkmove.SetInteger ("left", 0);
				movelinkmove.SetInteger ("right", 0);
				movelinkmove.SetInteger ("Speeed", 1);
				transform.position += Vector3.down * speed;// * Time.deltaTime;
			} else {
				movelinkmove.SetInteger ("down", 0);
				movelinkmove.SetInteger ("Up", 0);
				movelinkmove.SetInteger ("left", 0);
				movelinkmove.SetInteger ("right", 0);
				movelinkmove.SetInteger ("Speeed", 0);
			}
		}

	}


}
