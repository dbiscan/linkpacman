﻿using UnityEngine;
using System.Collections;

public class Rupee : MonoBehaviour {
	public Animator scoreText;
	public GameObject scorre;
	public int thescore;
	public int thescore2 = 0;
	public GameObject rupeeSound; 
	public AudioSource rupeS;
	public SpriteRenderer rupeeSprite;
	public bool deactivated = false;

	void Start(){

		rupeeSprite = this.GetComponent<SpriteRenderer> ();
		scorre = GameObject.Find ("linkWalkingPlain_0");
		scoreText = scorre.GetComponent<Animator> ();
	}

	void FixedUpdate () {
		thescore = scoreText.GetInteger ("ScoreCounter");
		if (thescore%5790==0) {
			rupeeSprite.sortingOrder = 1;
			deactivated = false;
		}
	}

	void Awake () {
		rupeeSound = GameObject.Find ("RupeeGameObject");
		rupeS = rupeeSound.GetComponent<AudioSource> ();
		
	}

	void OnTriggerEnter2D(Collider2D co) {
		if (co.name == "linkWalkingPlain_0" && deactivated==false) {
			deactivated = true;
			thescore2 = thescore+10;
			scoreText.SetInteger("ScoreCounter",thescore2);
			rupeS.Play();
			rupeeSprite.sortingOrder  = 0;

		}
	}
	
}
