﻿using UnityEngine;
using System.Collections;

public class StartMenu : MonoBehaviour {


	public GameObject StartW;
	public GameObject StartR;
	public GameObject ExitW;
	public GameObject ExitR;
	public GameObject CreditW;
	public GameObject CreditR;

	public SpriteRenderer StartW1;
	public SpriteRenderer StartR1;
	public SpriteRenderer ExitW1;
	public SpriteRenderer ExitR1;
	public SpriteRenderer CreditW1;
	public SpriteRenderer CreditR1;

	public int selection = 0;

	// Use this for initialization
	void Start () {

		Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);
	
		StartW = GameObject.Find ("StartTextWhit");
		StartR = GameObject.Find ("StartTextRed");
		ExitR = GameObject.Find ("ExitRed");
		ExitW  = GameObject.Find ("ExitWhite");
		CreditR = GameObject.Find ("CreditsRed");
		CreditW = GameObject.Find ("CreditsWhite");

		StartW1 = StartW.GetComponent<SpriteRenderer>();
		StartR1 = StartR.GetComponent<SpriteRenderer>();;
		ExitW1 = ExitW.GetComponent<SpriteRenderer>();;
		ExitR1 = ExitR.GetComponent<SpriteRenderer>();;
		CreditW1 = CreditW.GetComponent<SpriteRenderer>();;
		CreditR1 = CreditR.GetComponent<SpriteRenderer>();;
		selection = 0;
		CycleMenu ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.DownArrow)) {
			selection += 1;
			if (selection > 2) {
				selection = 0;
			}
			CycleMenu();
		} 
		if (Input.GetKey (KeyCode.UpArrow)) {
			selection -= 1;
			if (selection < 0) {
				selection = 2;
			}
			CycleMenu();
		} 
		if (Input.GetKey (KeyCode.Return)) {
			executeSelection ();
		}

	}

	void CycleMenu(){
		if (selection == 0) {
			StartW1.sortingOrder = 0;
			StartR1.sortingOrder = 3;
			ExitR1.sortingOrder = 0;
			ExitW1.sortingOrder = 3;
			CreditR1.sortingOrder = 0;
			CreditW1.sortingOrder = 3;
		} else if (selection == 1) {
			StartW1.sortingOrder = 3;
			StartR1.sortingOrder = 0;
			ExitR1.sortingOrder = 3;
			ExitW1.sortingOrder = 0;
			CreditR1.sortingOrder = 0;
			CreditW1.sortingOrder = 3;
		} else if (selection == 2) {
			StartW1.sortingOrder = 3;
			StartR1.sortingOrder = 0;
			ExitR1.sortingOrder = 0;
			ExitW1.sortingOrder = 3;
			CreditR1.sortingOrder = 3;
			CreditW1.sortingOrder = 0;
		}

		System.Threading.Thread.Sleep (100);
	}

	void executeSelection(){
		if (selection == 0) {
			Application.LoadLevel("Main");
		} else if (selection == 1) {
			Application.Quit();

		
		} else if (selection == 2) {
			Application.LoadLevel("Credits");
		
		}

	}
}
