﻿using UnityEngine;
using System.Collections;

public class scrollingCredits : MonoBehaviour {

	public GameObject Credits;
	public GUIText tc;
	public string creds;
	public float speed = 0.000000001f; 
	public GameObject Bgmusic;
	public AudioSource Music;

	// Use this for initialization
	void Start () {
		Bgmusic = GameObject.Find ("Cave");
		Music = Bgmusic.GetComponent<AudioSource> ();
		Music.Play ();
		Credits = GameObject.Find ("Creds");
		tc = Credits.GetComponent<GUIText>();
		Screen.SetResolution (Screen.currentResolution.width, Screen.currentResolution.height, true);

		creds = "Executive Producer: David Biscan\n\n"; 
		creds += "Art Director: David Biscan and a Llama\n\n"; 
		creds += "Technical Director: Caffine\n\n"; 
		creds += "Programming: David Biscan, caffine, and the internet\n\n"; 
		creds += "Level Design: David Biscan and more caffine\n\n";
		creds += "\n\nREFERNCES:\n";
		creds += "\n\nhttp://noproblo.dayjo.org/ZeldaSounds/                                                             ";
		creds += "\n\nhttp://forum.unity3d.com/threads/scrolling-credits-how.41828/                                      ";
		creds += "\n\nhttp://answers.unity3d.com/questions/180607/shortcut-key-to-maximize-viewport.html                 ";
		creds += "\n\nhttp://docs.unity3d.com/ScriptReference/Transform.Translate.html                                   ";
		creds += "\n\nhttp://images5.alphacoders.com/403/403996.jpg                                                      ";
		creds += "\n\nhttp://answers.unity3d.com/questions/600846/where-is-the-script-reference-for-order-in-layer-a.html";
		creds += "\n\nhttp://docs.unity3d.com/ScriptReference/Application.LoadLevel.html                                 ";
		creds += "\n\nhttp://www.unity3dstudent.com/2010/07/beginner-b16-switching-scenes/                               ";
		creds += "\n\nhttp://forum.unity3d.com/threads/how-to-switch-scenes-in-c.75455/                                  ";
		creds += "\n\nhttps://www.youtube.com/watch?v=0HwZQt94uHQ                                                        ";
		creds += "\n\nhttps://www.youtube.com/watch?v=buwlFYd7VQc                                                        ";
		creds += "\n\nhttp://www.dailymotion.com/video/x1yjz8l_add-score-in-2d-game-in-unity3d_tech                      ";
		creds += "\n\nhttp://docs.unity3d.com/ScriptReference/GameObject-layer.html                                      ";
		creds += "\n\nhttp://answers.unity3d.com/questions/588811/layers-and-collision-2d-1.html                         ";
		creds += "\n\nhttp://answers.unity3d.com/questions/756086/waiting-before-player-is-moved-to-spawn-c.html         ";
		creds += "\n\nhttp://docs.unity3d.com/ScriptReference/MonoBehaviour.OnDestroy.html                               ";
		creds += "\n\nhttps://www.youtube.com/watch?v=OvM5kW8PdGo                                                        ";
		creds += "\n\nhttp://answers.unity3d.com/questions/278883/how-do-i-make-my-player-die.html                       ";
		creds += "\n\nhttp://answers.unity3d.com/questions/407361/how-to-make-player-lose-lives.html                     ";
		creds += "\n\nhttp://img.photobucket.com/albums/v335/aabants/Sheet.png                                           ";
		creds += "\n\nhttp://docs.unity3d.com/ScriptReference/Collision2D.html                                           ";
		creds += "\n\nfile:///C:/Program%20Files/Unity/Editor/Data/Documentation/en/Manual/Animator.html                 ";
		creds += "\n\nhttp://docs.unity3d.com/Manual/Preparingacharacterfromscratch.html                                 ";
		creds += "\n\nhttp://docs.unity3d.com/Manual/MecanimFAQ.html                                                     ";
		creds += "\n\nhttps://unity3d.com/learn/tutorials/modules/beginner/2d/2d-controllers                             ";
		creds += "\n\nhttp://www.teomaragakis.com/game-development/creating-2d-zelda-clone-unity/2/                      ";
		creds += "\n\n\n\nPress Enter to return to the main menu!!!!                      ";
		tc.text = creds;

	}
	
	// Update is called once per frame
	void Update () {
		tc.transform.Translate(Vector3.up * speed * Time.deltaTime*0.5f);//, Camera.main.transform);
		if (Input.GetKey (KeyCode.Return)) {
			Application.LoadLevel ("Menu");
		}

	}
}
