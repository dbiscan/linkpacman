Read me

All instructions are on title screen.
All references are in credits.
BUGS:
For some reason the enemy animations aren�t updating. They actually turn the directions they�re supposed to but the animation doesn�t update. I�ve gone through all the settings and I can�t get it working again. They did work at one point then I noticed they weren�t anymore.
Scoring is a little weird. The score of one screen is not entirely consistent therefore I just went with the lowest option to refresh. Because of that I couldn�t properly have different levels. 
Sometimes the controls get �weird�. I don�t know why. Also the update of the directional animation sometimes doesn�t update quickly.
If you die back to back the animation doesn�t play even though it should.  
That�s about all I can think of.

